
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace VSModelCreator
{
    internal class MainWindow : Gtk.Window
    {
        [UI] private HeaderBar _headerBar = null;

        [UI] private Box _boxLeft = null;

        [UI] private Box _boxRight = null;

        [UI] private Box _boxCenter = null;

        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("_mainWindow"))
        {
            builder.Autoconnect(this);
            DeleteEvent += Window_DeleteEvent;
            ModelViewport vp = new(3, 3);
            _boxCenter.Add(vp);
            _boxCenter.ShowAll();
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }
    }
}
