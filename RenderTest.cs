using System;
using System.Drawing;
using Gdk;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Mathematics;
using VSModelCreator.Common;

namespace VSModelCreator
{
    // We now have a rotating rectangle but how can we make the view move based on the users input?
    // In this tutorial we will take a look at how you could implement a camera class
    // and start responding to user input.
    // You can move to the camera class to see a lot of the new code added.
    // Otherwise you can move to Load to see how the camera is initialized.

    // In reality, we can't move the camera but we actually move the rectangle.
    // This will explained more in depth in the web version, however it pretty much gives us the same result
    // as if the view itself was moved.
    public class RenderTest
    {
        private readonly float[] _vertices =
        {
            // Position         Texture coordinates
             0.5f,  0.5f, 0.0f, 1.0f, 1.0f, // top right
             0.5f, -0.5f, 0.0f, 1.0f, 0.0f, // bottom right
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, // bottom left
            -0.5f,  0.5f, 0.0f, 0.0f, 1.0f  // top left
        };

        private readonly uint[] _indices =
        {
            0, 1, 3,
            1, 2, 3
        };

        private int _elementBufferObject;

        private int _vertexBufferObject;

        private int _vertexArrayObject;

        private Shader _shader;

        private Texture _texture;

        private Texture _texture2;

        // The view and projection matrices have been removed as we don't need them here anymore.
        // They can now be found in the new camera class.

        // We need an instance of the new camera class so it can manage the view and projection matrix code.
        // We also need a boolean set to true to detect whether or not the mouse has been moved for the first time.
        // Finally, we add the last position of the mouse so we can calculate the mouse offset easily.
        private Camera _camera;

        private bool _firstMove = true;

        private Vector2 _lastPos;

        private double _time;

        const float cameraSpeed = 0.5f;

        const float sensitivity = 0.005f;

        private System.Drawing.Size size;

        public RenderTest(int width, int height)

        {
            size.Height = height;
            size.Width = width;
        }

        public void OnLoad()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.ClearColor(0.3f, 0.3f, 0.3f, 1.0f);

            GL.Enable(EnableCap.DepthTest);

            _vertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(_vertexArrayObject);

            _vertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * sizeof(float), _vertices, BufferUsageHint.StaticDraw);

            _elementBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _elementBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);

            _shader = new Shader("Shaders/shader.vert", "Shaders/shader.frag");
            _shader.Use();

            int vertexLocation = _shader.GetAttribLocation("aPosition");
            GL.EnableVertexAttribArray(vertexLocation);
            GL.VertexAttribPointer(vertexLocation, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);

            int texCoordLocation = _shader.GetAttribLocation("aTexCoord");
            GL.EnableVertexAttribArray(texCoordLocation);
            GL.VertexAttribPointer(texCoordLocation, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));

            _texture = Texture.LoadFromFile("Resources/container.png");
            _texture.Use(TextureUnit.Texture0);

            // _texture2 = Texture.LoadFromFile("Resources/awesomeface.png");
            // _texture2.Use(TextureUnit.Texture1);

            _shader.SetInt("texture0", 0);
            // _shader.SetInt("texture1", 1);

            // We initialize the camera so that it is 3 units back from where the rectangle is.
            // We also give it the proper aspect ratio.
            _camera = new Camera(Vector3.UnitZ, size.Width / (float)size.Height);
        }

        internal void OnResize(int width, int height)
        {
            _camera.AspectRatio = height / (float)width;
        }

        public void OnRenderFrame(double delta)
        {

            _time += delta / 10_000_000_000;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.BindVertexArray(_vertexArrayObject);

            _texture.Use(TextureUnit.Texture0);
            // _texture2.Use(TextureUnit.Texture1);
            _shader.Use();

            Matrix4 model = Matrix4.Identity * Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(_time));
            _shader.SetMatrix4("model", model);
            _shader.SetMatrix4("view", _camera.GetViewMatrix());
            _shader.SetMatrix4("projection", _camera.GetProjectionMatrix());

            GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);

            //SwapBuffers();
        }
        public void OnKeyPressEvent(EventKey evnt, float time)
        {
            if (evnt.KeyValue == (int)Key.w)
            {
                _camera.Position += _camera.Front * cameraSpeed * time; // Forward
            }

            if (evnt.KeyValue == (int)Key.s)
            {
                _camera.Position -= _camera.Front * cameraSpeed * time; // Backwards
            }
            if (evnt.KeyValue == (int)Key.a)
            {
                _camera.Position -= _camera.Right * cameraSpeed * time; // Left
            }
            if (evnt.KeyValue == (int)Key.d)
            {
                _camera.Position += _camera.Right * cameraSpeed * time; // Right
            }
            if (evnt.KeyValue == (int)Key.space)
            {
                _camera.Position += _camera.Up * cameraSpeed * time; // Up
            }
            if (evnt.KeyValue == (int)Key.Shift_L)
            {
                _camera.Position -= _camera.Up * cameraSpeed * time; // Down
            }


        }

        public void OnMouse(float x, float y)
        {

            if (_firstMove) // This bool variable is initially set to true.
            {
                _lastPos = new Vector2(x, y);
                _firstMove = false;
            }
            else
            {
                // Calculate the offset of the mouse position
                float deltaX = y - _lastPos.X;
                float deltaY = x - _lastPos.Y;
                _lastPos = new Vector2(x, y);

                // Apply the camera pitch and yaw (we clamp the pitch in the camera class)
                _camera.Yaw += deltaX * sensitivity;
                _camera.Pitch -= deltaY * sensitivity; // Reversed since y-coordinates range from bottom to top
            }
        }
    }
}