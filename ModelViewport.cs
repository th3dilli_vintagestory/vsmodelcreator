using System;
using Cairo;
using Gdk;
using Gtk;
using OpenTK.Graphics.OpenGL4;

namespace VSModelCreator
{
    internal class ModelViewport : GLArea
    {

        RenderTest renderTest;


        public ModelViewport(int glVersionMajor, int glVersionMinor)
        {
            renderTest = new RenderTest(Screen.Width, Screen.Height);

            SetRequiredVersion(glVersionMajor, glVersionMinor);
            AutoRender = true;
            CanFocus = true;
            Expand = true;
            Events |=
                EventMask.AllEventsMask;
        }


        protected override GLContext OnCreateContext()
        {
            Console.WriteLine("OnCreateContext");
            GLContext context = Window.CreateGlContext();

            GetRequiredVersion(out int major, out int minor);
            context.SetRequiredVersion(major, minor);

            context.Realize();
            return context;
        }

        protected override bool OnRender(GLContext context)
        {
            // MakeCurrent();
            // GL.Clear(ClearBufferMask.ColorBufferBit);
            // GL.ClearColor(1f, 0.3f, 0.3f, 1.0f);

            // Console.WriteLine("OnRender");

            renderTest.OnRenderFrame(FrameClock.CurrentTimings.FrameTime);
            // Console.WriteLine("FrameTime: " + FrameClock.CurrentTimings.FrameTime / 10_000_000);
            QueueDraw();

            return base.OnRender(context);
        }

        protected override void OnShown()
        {
            base.OnShown();
            Console.WriteLine("OnShown");
        }

        protected override void OnRealized()
        {
            base.OnRealized();
            MakeCurrent();

            GL.LoadBindings(new BindingContext());
            renderTest.OnLoad();
            Console.WriteLine("OnRealized");
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            Console.WriteLine("OnActivate");
        }

        protected override void OnResize(int width, int height)
        {
            base.OnResize(width, height);
            GL.Viewport(0, 0, width, height);
            // We need to update the aspect ratio once the window has been resized.
            //_camera.AspectRatio = args.Height / (float)args.Width;
            renderTest.OnResize(width, height);
            Console.WriteLine("OnResize");
        }

        protected override bool OnDrawn(Context cr)
        {
            // Console.WriteLine("OnDrawn");
            return base.OnDrawn(cr);
        }

        protected override bool OnFocused(DirectionType direction)
        {
            Console.WriteLine("OnFocused");
            return base.OnFocused(direction);
        }

        protected override bool OnKeyPressEvent(EventKey evnt)
        {
            Console.WriteLine("OnKeyPressEvent: " + evnt.KeyValue.ToString());
            renderTest.OnKeyPressEvent(evnt, 1);
            return base.OnKeyPressEvent(evnt);
        }
        protected override bool OnKeyReleaseEvent(EventKey evnt)
        {
            Console.WriteLine("OnKeyReleaseEvent: " + evnt.KeyValue.ToString());
            return base.OnKeyReleaseEvent(evnt);
        }

        protected override bool OnMotionNotifyEvent(EventMotion evnt)
        {
            // Console.WriteLine("OnMotionNotifyEvent: " + evnt.X + " | " + evnt.Y);
            // renderTest.OnMouse((float)evnt.X, (float)evnt.Y);
            return base.OnMotionNotifyEvent(evnt);
        }
    }
}