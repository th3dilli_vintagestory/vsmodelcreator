﻿using System;
using Gtk;

namespace VSModelCreator
{
    public static class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            Application app = new("at.VintageStory.ModelCreator", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            MainWindow win = new();
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
